﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IProdutoUsuarioService : IServiceBase<ProdutoUsuario>
    {
        IEnumerable<ProdutoUsuario> BuscaPorUsuario(Usuario usuario);
        IEnumerable<ProdutoUsuario> BuscaPorProduto(int id,Usuario usuario);

        IEnumerable<ProdutoUsuario> BuscaPorNome(string termo, Usuario usuario);
    }
}
