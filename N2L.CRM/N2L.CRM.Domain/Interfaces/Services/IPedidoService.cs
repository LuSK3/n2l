﻿using System.Collections.Generic;
using System.Linq;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IPedidoService:IServiceBase<Pedido>
    {
        IEnumerable<Pedido> BuscaPedidosUsuario(Usuario usuario);
    }
}
