﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IProdutoService:IServiceBase<Produto>
    {
        IEnumerable<Produto> BuscaPorNome(string nome);
    }
}
