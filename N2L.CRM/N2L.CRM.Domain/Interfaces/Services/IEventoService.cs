﻿using System.Collections;
using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IEventoService :IServiceBase<Eventos>
    {
        IEnumerable<Eventos> GetEventosUsers(Eventos eventos);
    }
}
