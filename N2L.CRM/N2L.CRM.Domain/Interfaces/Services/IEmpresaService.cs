﻿using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IEmpresaService:IServiceBase<Empresa>
    {
    }
}
