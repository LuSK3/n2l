﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IClienteService:IServiceBase<Cliente>
    {
        IEnumerable<Cliente> BuscaClienteUsuario(string nome, Usuario usuario);
    }
}
