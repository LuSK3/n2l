﻿using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Services
{
    public interface IUsuarioService:IServiceBase<Usuario>
    {
        Usuario BuscaPorLogin(string Nome, string Senha);

        Usuario BuscaLogin(string Login);
    }
}
