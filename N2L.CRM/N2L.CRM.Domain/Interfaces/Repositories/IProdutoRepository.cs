﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IProdutoRepository:IRepositoryBase<Produto>
    {
        IEnumerable<Produto> BuscaPorNome(string nome);
    }
}
