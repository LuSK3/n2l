﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IProdutoUsuarioRepository : IRepositoryBase<ProdutoUsuario>
    {
        IEnumerable<ProdutoUsuario> BuscaPorUsuario(Usuario user);

        IEnumerable<ProdutoUsuario> BuscaPorProduto(int id,Usuario usuario);

        IEnumerable<ProdutoUsuario> BuscaPorNome(string termo, Usuario usuario);

    }
}
