﻿using System.Collections.Generic;
using System.Linq;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IPedidoRepository:IRepositoryBase<Pedido>
    {
        IEnumerable<Pedido> BuscaPedidosUsuario(Usuario usuario);
    }
}
