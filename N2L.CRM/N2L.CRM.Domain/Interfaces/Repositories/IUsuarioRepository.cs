﻿using System.Collections;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        Usuario BuscaPorLogin(string login, string senha);

        Usuario BuscaLogin(string Login);
    }
}
