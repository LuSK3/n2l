﻿using System.Collections;
using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IClienteRepository : IRepositoryBase<Cliente>
    {
        IEnumerable<Cliente> BuscaClienteUsuario(string nome, Usuario usuario);
    }
}
