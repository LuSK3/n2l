﻿using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IEmpresaRepository : IRepositoryBase<Empresa>
    {

    }
}
