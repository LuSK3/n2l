﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Domain.Interfaces.Repositories
{
    public interface IEventoRepository :IRepositoryBase<Eventos>
    {
        IEnumerable<Eventos> GetEventosUsers(Eventos eventos);
    }
}
