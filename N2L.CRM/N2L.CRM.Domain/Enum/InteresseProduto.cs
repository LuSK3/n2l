﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N2L.CRM.Domain.Enum
{
    public enum InteresseProduto:byte
    {
        Pele,
        Maquiagem,
        Fragrância,
        Corpo,
        Masulino
    }
}
