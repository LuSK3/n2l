﻿

using System.ComponentModel;
namespace N2L.CRM.Domain.Enum
{
    public enum TipoContato :byte
    {
        Telefone,
        [Description("E-Mail")]
        Email,
        Pessoalmente,
        [Description("Mídias Sociais")]
        MidiasSociais
    };
}
