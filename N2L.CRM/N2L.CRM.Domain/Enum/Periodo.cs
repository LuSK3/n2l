﻿namespace N2L.CRM.Domain.Enum
{
    public enum Periodo : byte
    {
        Manhã,
        Tarde,
        Noite
    }
}
