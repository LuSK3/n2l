﻿using System.Collections.Generic;
using System.Linq;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Domain.Services
{
    public class PedidoService : ServiceBase<Pedido>, IPedidoService
    {
        private readonly IPedidoRepository _pedidoRepository;

        public PedidoService(IPedidoRepository pedidoRepository)
            : base(pedidoRepository)
        {
            _pedidoRepository = pedidoRepository;
        }

        public IEnumerable<Pedido> BuscaPedidosUsuario(Usuario usuario)
        {
            return _pedidoRepository.BuscaPedidosUsuario(usuario);
        }
    }
}
