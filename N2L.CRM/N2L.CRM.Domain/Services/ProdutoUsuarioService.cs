﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Domain.Services
{
    public class ProdutoUsuarioService : ServiceBase<ProdutoUsuario>, IProdutoUsuarioService
    {
        private readonly IProdutoUsuarioRepository _produtoUsuarioRepository;

        public ProdutoUsuarioService(IProdutoUsuarioRepository produtoUsuarioRepository)
            : base(produtoUsuarioRepository)
        {
            this._produtoUsuarioRepository = produtoUsuarioRepository;
        }

        public IEnumerable<ProdutoUsuario> BuscaPorUsuario(Usuario usuario)
        {
            return _produtoUsuarioRepository.BuscaPorUsuario(usuario);
        }

        public IEnumerable<ProdutoUsuario> BuscaPorProduto(int id, Usuario usuario)
        {
            return _produtoUsuarioRepository.BuscaPorProduto(id,usuario);
        }





        public IEnumerable<ProdutoUsuario> BuscaPorNome(string termo, Usuario usuario)
        {
            return _produtoUsuarioRepository.BuscaPorNome(termo, usuario);
        }
    }
}
