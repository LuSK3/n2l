﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Domain.Services
{                                        
    public class ItemPedidoService :ServiceBase<ItemPedido>,IItemPedidoService
    {
        private readonly IItemPedidoRepository _itemPedidoRepository;

        public ItemPedidoService(IItemPedidoRepository itemPedidoRepository)
            :base(itemPedidoRepository)
        {
            _itemPedidoRepository = itemPedidoRepository;
        }
    }
}
