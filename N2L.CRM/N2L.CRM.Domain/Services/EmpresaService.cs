﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Domain.Services
{
    public class EmpresaService : ServiceBase<Empresa>, IEmpresaService
    {
        private readonly IEmpresaRepository _empresaRepository;

        public EmpresaService(IEmpresaRepository empresaRepository)
            : base(empresaRepository)
        {
            _empresaRepository = empresaRepository;
        }
    }
}
