﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace N2L.CRM.Domain.Services
{
    public class EventoService : ServiceBase<Eventos>, IEventoService
    {
        private readonly IEventoRepository _eventoRepository;

        public EventoService(IEventoRepository eventoRepository)
            :base(eventoRepository)
        {
            this._eventoRepository = eventoRepository;
        }

        public IEnumerable<Eventos> GetEventosUsers(Eventos eventos)
        {
            return _eventoRepository.GetEventosUsers(eventos);
        }
    }
}
