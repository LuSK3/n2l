﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Domain.Services
{
    public class UsuarioService:ServiceBase<Usuario>,IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
            :base(usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public Usuario BuscaPorLogin(string login, string senha)
        {
            return _usuarioRepository.BuscaPorLogin(login, senha);
        }


        public Usuario BuscaLogin(string Login)
        {
            return _usuarioRepository.BuscaLogin(Login);
        }
    }
}
