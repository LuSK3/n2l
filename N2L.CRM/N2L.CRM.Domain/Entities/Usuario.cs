﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace N2L.CRM.Domain.Entities
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        
        public string Nome { get; set; }

        
        public string Login { get; set; }
        public string Senha { get; set; }

        public string Matricula { get; set; }


        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Pais{ get; set; }

        public string CEP { get; set; }


        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Facebook { get; set; }
        public string Email { get; set; }

        public string CPF { get; set; }

        public string Foto { get; set; }

        public int CodigoSupervisor { get; set; }

        public int EmpresaId { get; set; }
        public virtual Empresa Empresa { get; set; }
    }
}
