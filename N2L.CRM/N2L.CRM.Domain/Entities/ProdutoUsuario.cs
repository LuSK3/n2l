﻿
namespace N2L.CRM.Domain.Entities
{
    public class ProdutoUsuario
    {
        public int ProdutoUsuarioId { get; set; }
        public decimal QtdEstoque { get; set; }
        public int ProdutoId { get; set; }
        public int UsuarioId { get; set; }
        public virtual Produto Produto { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
