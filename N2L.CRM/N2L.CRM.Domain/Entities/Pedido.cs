﻿using System;
using System.Collections.Generic;

namespace N2L.CRM.Domain.Entities
{
    public class Pedido
    {
        public int PedidoId { get; set; }
        public int ClienteId { get; set; }
        public Boolean Status { get; set; }
        public decimal ProdutoQtde { get; set; }
        public DateTime DataPedido { get; set; }
        public DateTime DataEntrega { get; set; }
        public int ProdutoId { get; set; }    
        public int UsuarioId { get; set; }

        public virtual Usuario Usuario { get; set; }   
        public virtual Produto Produto { get; set; }
        public virtual Cliente Cliente  { get; set; }
    }
}
