﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N2L.CRM.Domain.Entities
{
    public class ItemPedido
    {
        public int ItemPedidoId { get; set; }
        public decimal ProdutoQtde { get; set; }
        public int ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }
        public int PedidoId { get; set; }
        public virtual Pedido Pedido { get; set; }
    }
}
