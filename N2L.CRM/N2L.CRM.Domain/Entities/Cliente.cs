﻿using System;
using System.Collections.Generic;
using N2L.CRM.Domain.Enum;

namespace N2L.CRM.Domain.Entities
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Email { get; set; }
        public Cliente Anfitriã { get; set; }
        public string Observacao { get; set; }


        public Periodo PeriodoContato { get; set; }
        public TipoContato TipoContato { get; set; }
        public InteresseProduto InteresseProduto { get; set; }
        public TomDaPele TomDaPele { get; set; }
        public TipoPele TipoPele { get; set; }


        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Facebook { get; set; }


        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string Pais { get; set; }

        public virtual IEnumerable<Pedido> Pedidos { get; set; }
        public bool Status { get; set; }
        public DateTime DataCadastro { get; set; }

        public int UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }


    }
}
