﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N2L.CRM.Domain.Entities
{
    public class Eventos
    {
        public int EventoId { get; set; }
        public int UsuarioId { get; set; }
        public int EmpresaId { get; set; }
        public string Descricao { get; set; }
        public DateTime DtEventoInicio { get; set; }
        public DateTime DtEventoFinal { get; set; }
        public bool Status { get; set; }
        public string Color { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Empresa Empresa{ get; set; }

    }
}
