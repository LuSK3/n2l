﻿
namespace N2L.CRM.Domain.Entities
{
    public class Produto
    {
        public int ProdutoId { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public bool Disponivel { get; set; }
        public int EmpresaId { get; set; } 
        public virtual Empresa Empresa { get; set; }
        

    }
}
