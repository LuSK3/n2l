﻿using System.Collections.Generic;

namespace N2L.CRM.Domain.Entities
{
    public class Empresa
    {
        public int EmpresaId { get; set; }
        public string Descricao { get; set; }

        public string CNPJ { get; set; }

        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Pais { get; set; }
        public string CEP { get; set; }
        public string Telefone { get; set; }
        public string TelefoneRecado { get; set; }
        public string Site { get; set; }
        

        public virtual IEnumerable<Usuario> Usuarios { get; set; }
    }
}
