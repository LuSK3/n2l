﻿using System.Linq;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using System.Collections.Generic;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class ProdutoUsuarioRepository:RepositoryBase<ProdutoUsuario>,IProdutoUsuarioRepository
    {
        public IEnumerable<ProdutoUsuario> BuscaPorUsuario(Usuario user)
        {
            return db.ProdutoUsuario.Where(p => p.UsuarioId == user.UsuarioId);
        }

        public IEnumerable<ProdutoUsuario> BuscaPorProduto(int id, Usuario usuario)
        {
            return db.ProdutoUsuario.Where(p => p.ProdutoId == id && p.UsuarioId == usuario.UsuarioId);
        }


        public IEnumerable<ProdutoUsuario> BuscaPorNome(string termo, Usuario usuario)
        {
            return db.ProdutoUsuario.Where(p => p.Produto.Descricao.Contains(termo) && p.UsuarioId == usuario.UsuarioId);
        }
    }
}
