﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class ProdutoRepository : RepositoryBase<Produto>, IProdutoRepository
    {

        public IEnumerable<Produto> BuscaPorNome(string nome)
        {
            return db.Produto.Where(p => p.Descricao.Contains(nome));
        }
    }
}
