﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class ItemPedidoRepository:RepositoryBase<ItemPedido>, IItemPedidoRepository
    {
        /*Implementa interface que não existe no repositório genérico*/
    }
}
