﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class ClienteRepository:RepositoryBase<Cliente>,IClienteRepository
    {
        /*Implementa interface que não existe no repositório genérico*/
        public IEnumerable<Cliente> BuscaClienteUsuario(string nome, Usuario usuario)
        {
            return db.Cliente.Where(p => p.UsuarioId == usuario.UsuarioId 
                                      && p.Status == true 
                                      && p.Nome.Contains(nome));
        }
    }
}
