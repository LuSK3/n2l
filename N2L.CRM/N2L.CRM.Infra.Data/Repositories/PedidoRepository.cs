﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class PedidoRepository:RepositoryBase<Pedido>,IPedidoRepository
    {
        public IEnumerable<Pedido> BuscaPedidosUsuario(Usuario usuario)
        {
            var dbs = db.Pedido.Where(x => x.UsuarioId == usuario.UsuarioId);
            return dbs;
        }
    }
}
