﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class EmpresaRepository :RepositoryBase<Empresa>,IEmpresaRepository
    {

    }
}
