﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class EventoRepository : RepositoryBase<Eventos>, IEventoRepository
    {
        public IEnumerable<Eventos> GetEventosUsers(Eventos eventos)
        {
            return db.Eventos.Where(x => x.DtEventoInicio >= eventos.DtEventoInicio
                                         && x.DtEventoFinal <= eventos.DtEventoFinal
                                         && x.UsuarioId == eventos.Usuario.UsuarioId).ToList();
        }
    }
}
