﻿using System.Linq;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Repositories;

namespace N2L.CRM.Infra.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {

        public Usuario BuscaPorLogin(string login, string senha)
        {
            return db.Usuario.FirstOrDefault(x => x.Login == login && x.Senha == senha);

         }     

        public Usuario BuscaLogin(string Login)
        {
            return db.Usuario.FirstOrDefault(x => x.Login == Login);
        }
    }
}
