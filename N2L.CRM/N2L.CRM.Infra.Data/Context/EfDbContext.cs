﻿using N2L.CRM.Domain.Entities;
using N2L.CRM.Infra.Data.EntityConfig;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;

namespace N2L.CRM.Infra.Data.Context
{
    public class EfDbContext:DbContext
    {
        public EfDbContext()
            //:base("EfDbConnSmarter")
            : base("EfDbConnection")
            //: base("EfDbConnAzure")
        {
            
        }

        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
        //public DbSet<ItemPedido> ItemPedido { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Eventos> Eventos { get; set; }

        public DbSet<ProdutoUsuario> ProdutoUsuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(x=>x.Name == x.ReflectedType.Name + "Id")
                .Configure(p=>p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(p=>p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(x=>x.HasMaxLength(100));

            

            //this.Database.ExecuteSqlCommand("ALTER TABLE USUARIO")

            modelBuilder.Configurations.Add(new ClienteConfig());
            modelBuilder.Configurations.Add(new ProdutoConfig());
            //modelBuilder.Configurations.Add(new ItemPedidoConfig());
            modelBuilder.Configurations.Add(new PedidoConfig());
            modelBuilder.Configurations.Add(new EmpresaConfig());
            modelBuilder.Configurations.Add(new UsuarioConfig());
            modelBuilder.Configurations.Add(new EventoConfig());

        }
        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry=>entry.Entity.GetType().GetProperty("DataCadastro")!= null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = false;
                }
            }
            return base.SaveChanges();
        }


    }

    

}
