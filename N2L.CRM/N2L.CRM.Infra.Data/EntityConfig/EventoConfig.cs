﻿using N2L.CRM.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class EventoConfig :EntityTypeConfiguration<Eventos>
    {
        public EventoConfig()
        {
            HasKey(p => p.EventoId);
            Property(p => p.Descricao)
                .IsRequired()
                .HasMaxLength(200);

            HasRequired(p => p.Empresa)
                .WithMany()
                .HasForeignKey(p => p.EmpresaId);

            HasRequired(p => p.Usuario)
                .WithMany()
                .HasForeignKey(p => p.UsuarioId);

        }
    }
}
