﻿using N2L.CRM.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class ProdutoUsuarioConfig:EntityTypeConfiguration<ProdutoUsuario>
    {
        public ProdutoUsuarioConfig()
        {
            HasKey(c => c.ProdutoUsuarioId);
            
            HasRequired(c => c.Produto)
                .WithMany()
                .HasForeignKey(c => c.ProdutoId);

            HasRequired(c => c.Usuario)
                .WithMany()
                .HasForeignKey(c => c.UsuarioId);
        }
    }
}
