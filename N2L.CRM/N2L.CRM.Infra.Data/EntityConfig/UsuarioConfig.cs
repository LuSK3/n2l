﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Annotations;
using N2L.CRM.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class UsuarioConfig:EntityTypeConfiguration<Usuario>
    {
        public UsuarioConfig()
        {
            HasKey(c => c.UsuarioId);
            HasRequired(c => c.Empresa)
                .WithMany()
                .HasForeignKey(c => c.EmpresaId);

            Property(x => x.CEP).HasMaxLength(20);
            Property(x => x.UF).HasMaxLength(2);

            Property(x => x.Login)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute(){IsUnique = true}));

        }
    }
}
