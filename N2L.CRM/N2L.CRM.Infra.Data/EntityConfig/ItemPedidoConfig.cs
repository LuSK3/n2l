﻿using N2L.CRM.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class ItemPedidoConfig:EntityTypeConfiguration<ItemPedido>
    {
        public ItemPedidoConfig()
        {
            HasKey(c => c.ItemPedidoId)
                .HasRequired(c => c.Pedido)
                .WithMany()
                .HasForeignKey(c => c.PedidoId);

            HasKey(c => c.ItemPedidoId)
                .HasRequired(c => c.Produto)
                .WithMany()
                .HasForeignKey(c => c.ProdutoId);


        }
    }
}
