﻿using N2L.CRM.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    class EmpresaConfig :EntityTypeConfiguration<Empresa>
    {
        public EmpresaConfig()
        {
            HasKey(c => c.EmpresaId);
            Property(c => c.Descricao)
                .IsRequired()
                .HasMaxLength(200);
        }
    }
}
