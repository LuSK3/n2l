﻿using N2L.CRM.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class ProdutoConfig : EntityTypeConfiguration<Produto>
    {
        public ProdutoConfig()
        {
            HasKey(c => c.ProdutoId)
                .HasRequired(c => c.Empresa)
                .WithMany()
                .HasForeignKey(c => c.EmpresaId);
        }
    }
}
