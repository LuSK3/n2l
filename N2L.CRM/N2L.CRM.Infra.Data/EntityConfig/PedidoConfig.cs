﻿using N2L.CRM.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class PedidoConfig:EntityTypeConfiguration<Pedido>
    {
        public PedidoConfig()
        {
            HasKey(c => c.PedidoId);

            HasRequired(c => c.Cliente)
                .WithMany()
                .HasForeignKey(c => c.ClienteId);

            HasRequired(p => p.Produto)
                .WithMany()
                .HasForeignKey(p => p.ProdutoId);

            HasRequired(p => p.Usuario)
                .WithMany()
                .HasForeignKey(p => p.UsuarioId);
        } 
    }
}
