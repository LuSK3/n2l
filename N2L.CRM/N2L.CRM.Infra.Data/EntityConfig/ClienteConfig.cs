﻿
using System.Data.Entity.ModelConfiguration;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Infra.Data.EntityConfig
{
    public class ClienteConfig : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfig()
        {
            HasKey(c => c.ClienteId);
            Property(c => c.Nome).IsRequired().HasMaxLength(150);
            Property(c => c.Sobrenome).IsRequired().HasMaxLength(200);
            Property(c => c.Email).IsRequired();
            Property(c => c.UF).HasMaxLength(2);
            Property(c => c.Observacao).HasMaxLength(800);

            HasRequired(c => c.Usuario)
                .WithMany()
                .HasForeignKey(c => c.UsuarioId); 
        }
    }
}
