﻿using N2L.CRM.Application.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace N2L.CRM.WEB.UI.Controllers
{
    public class ValidacaoController : Controller
    {
        private readonly IUsuarioAppService _usuarioApp;
        public ValidacaoController(IUsuarioAppService usuarioApp)
        {
            this._usuarioApp = usuarioApp;
        }


        public JsonResult ValidaLogin(string Login)
        {
            if (_usuarioApp.BuscaLogin(Login) != null)
                return Json(String.Format("{0} Login não disponível", Login), JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}