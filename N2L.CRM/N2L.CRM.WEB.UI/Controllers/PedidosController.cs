﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;

namespace N2L.CRM.WEB.UI.Controllers
{
    public class PedidosController : Controller
    {
        private readonly IPedidoAppService _pedidoApp;
        private readonly IProdutoUsuarioAppService _produtoUsuarioApp;
        private readonly IProdutoAppService _produtoApp;
        private readonly IClienteAppService _clienteApp;

        public PedidosController(IPedidoAppService pedidoAppService,
                                 IClienteAppService clienteAppService,
                                 IProdutoUsuarioAppService produtoUsuarioApp,
                                 IProdutoAppService produtoApp
            )
        {
            _clienteApp = clienteAppService;
            _pedidoApp = pedidoAppService;
            _produtoUsuarioApp = produtoUsuarioApp;
            _produtoApp = produtoApp;
        }


        // GET: Pedidos
        public ActionResult Index()
        {
            var pedidoViewModel = Mapper.Map<IEnumerable<Pedido>, IEnumerable<PedidoViewModel>>(_pedidoApp.GetAll());

            return View(pedidoViewModel);
        }

        // GET: Pedidos/Details/5
        public ActionResult Details(int id)
        {
            var pedido = _pedidoApp.GetbyId(id);
            var pedidoViewModel = Mapper.Map<Pedido, PedidoViewModel>(pedido);

            return View();
        }

        // GET: Pedidos/Create
        public ActionResult Create()
        {
            ViewBag.Clientes = _clienteApp.GetAll();
            var pedidos = Mapper.Map<IEnumerable<Pedido>,IEnumerable<PedidoViewModel>>(_pedidoApp.GetAll());
            return View(pedidos);
        }

        // POST: Pedidos/Create
        [HttpPost, ActionName("Create")]
        //[ValidateAntiForgeryToken]
        public ActionResult Creates(string produto, string cliente, string quantidade)
        {
            if (String.IsNullOrEmpty(produto)
                || String.IsNullOrEmpty(cliente)
                || String.IsNullOrEmpty(quantidade)
                )
            {
                Response.StatusCode = 999;
                return Content("verifique as informações");
            }
            else
            {
                var usuario = (Usuario)Session["usuario"];
                var pedido = new Pedido()
                {
                    ProdutoId = Convert.ToInt32(produto),
                    //Cliente = _clienteApp.GetbyId(Convert.ToInt32(cliente)),
                    ClienteId = Convert.ToInt32(cliente),
                    //Produto = _produtoApp.GetbyId(Convert.ToInt32(produto)),
                    ProdutoQtde = Convert.ToDecimal(quantidade),
                    Status = true,
                    DataPedido = DateTime.Now,
                    DataEntrega = Convert.ToDateTime("1900-01-01"),
                    //Usuario = usuario,
                    UsuarioId = usuario.UsuarioId
                };
                var produtoUsuario = _produtoUsuarioApp.BuscaPorProdutos(Convert.ToInt32(produto), (Usuario)Session["usuario"]).FirstOrDefault();
                if (produtoUsuario != null)
                {
                    produtoUsuario.QtdEstoque = produtoUsuario.QtdEstoque - pedido.ProdutoQtde;
                    _produtoUsuarioApp.Update(produtoUsuario);
                    _pedidoApp.Add(pedido);
                    var order = _pedidoApp.BuscaPedidosUsuario(usuario).Last();
                    pedido.PedidoId = order.PedidoId;
                    pedido.Produto = _produtoApp.GetbyId(Convert.ToInt32(produto));
                    pedido.Cliente = _clienteApp.GetbyId(Convert.ToInt32(cliente));

                    return Json(pedido, JsonRequestBehavior.AllowGet);
                }

            }
            return View();
        }

        // GET: Pedidos/Edit/5
        public ActionResult EditPedido(int id)
        {
            var pedidoDomain = _pedidoApp.GetbyId(id);
            var pedidoModel = Mapper.Map<Pedido, PedidoViewModel>(pedidoDomain);

            return PartialView("_EditPedido",pedidoModel);
        }

        // POST: Pedidos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pedidos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Pedidos/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult Deletes(int id)
        {
            var pedidoDomain = _pedidoApp.GetbyId(id);
            _pedidoApp.Remove(pedidoDomain);
            return Content("");
        }

        [HttpGet]
        public PartialViewResult CreateItemPedido()
        {
            return PartialView("_CreatePedido", new PedidoViewModel());
        }

        [HttpGet]
        public ActionResult AutoCompleteProdutos(string term)
        {
            var produtoUsuarioDomain = _produtoUsuarioApp.BuscaPorNome(term, (Usuario)Session["Usuario"]).ToList();

            var produtoss = produtoUsuarioDomain.Select(produtoUsuario => produtoUsuario.Produto).ToList();

            if (produtoss != null)
            {
                var listaProdutosDomain = produtoss;
                var produtos = Mapper.Map<IEnumerable<Produto>, IEnumerable<ProdutoViewModel>>(listaProdutosDomain);
                var arr = produtos.Select(x => new { Nome = x.Descricao, Codigo = x.ProdutoId }).ToList();
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content("Produto inválido");
            }

        }

        public ActionResult GetDisponibility(int id, string quantidade)
        {

            if (id <= 0 || quantidade == "0")
            {
                return Content("Produto fora de estoque");
            }

            var qtd = Convert.ToDecimal(quantidade);
            var produtoUsuarioDomain = _produtoUsuarioApp.BuscaPorProdutos(id, (Usuario)Session["Usuario"]).FirstOrDefault();


            if (qtd <= produtoUsuarioDomain.QtdEstoque)
            {
                Response.StatusCode = 998;
                return Content("Produto Inserido");
            }
            else
            {
                Response.StatusCode = 999;
                return Content("Produto fora de estoque");
            }

        }
        public ActionResult AutoCompleteClientes(string term)
        {
            var clientesDomain = _clienteApp.BuscaPorClienteUsuario(term, (Usuario)Session["usuario"]);

            if (clientesDomain != null)
            {
                var clientesViewModel = Mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteViewModel>>(clientesDomain);

                var json = clientesViewModel.Select(x => new { Nome = x.Nome, Codigo = x.ClienteId }).ToList();

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content("Cliente não encontrado");
            }

        }

    }
}
