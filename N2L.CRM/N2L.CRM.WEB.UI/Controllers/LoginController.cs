﻿using System;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;
using System.Web.Mvc;
using System.Web.Security;

namespace N2L.CRM.WEB.UI.Controllers
{
    public class LoginController : Controller
    {

        private readonly IUsuarioAppService _usuarioApp;


        public LoginController(IUsuarioAppService usuarioApp)
        {
            _usuarioApp = usuarioApp;
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UsuarioViewModel userModel)
        {
            var usuarioDomain = _usuarioApp.BuscaPorLogin(userModel.Login, userModel.Senha);
            var userViewModel = Mapper.Map<Usuario, UsuarioViewModel>(usuarioDomain);

            Session["Usuario"] = usuarioDomain;

            if (userViewModel != null)
            {
                // userViewModel.Foto);

                HttpRuntime.Cache.Insert("Foto", userViewModel.Foto, null, DateTime.Now.AddHours(1), Cache.NoSlidingExpiration);
                HttpRuntime.Cache.Insert("Codigo", userViewModel.UsuarioId, null, DateTime.Now.AddHours(1), Cache.NoSlidingExpiration);
                HttpRuntime.Cache.Insert("Usuario", userViewModel.Nome, null, DateTime.Now.AddHours(1), Cache.NoSlidingExpiration);



                FormsAuthentication.SetAuthCookie(userViewModel.Login, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["validacao"] = "Login ou senha incorretos";
                return View(userModel);
            }
        }
    }
}