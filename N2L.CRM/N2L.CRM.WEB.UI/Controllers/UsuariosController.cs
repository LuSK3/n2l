﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;


namespace N2L.CRM.WEB.UI.Controllers
{
    [Authorize]
    public class UsuariosController : Controller
    {

        private readonly IUsuarioAppService _usuarioApp;
        private readonly IEmpresaAppService _empresaApp;

        public UsuariosController(IUsuarioAppService usuarioAppService, IEmpresaAppService empresaAppService)
        {
            _usuarioApp = usuarioAppService;
            _empresaApp = empresaAppService;
        }

        public UsuariosController(IUsuarioAppService usuarioAppService)
        {
            _usuarioApp = usuarioAppService;
        }


        // GET: Usuarios
        public ActionResult Index()
        {
            var usuarioViewModel = Mapper.Map<IEnumerable<Usuario>, IEnumerable<UsuarioViewModel>>(_usuarioApp.GetAll());

            return View(usuarioViewModel);
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int id)
        {
            var usuario = _usuarioApp.GetbyId(id);
            var usuarioViewModel = Mapper.Map<Usuario, UsuarioViewModel>(usuario);

            return View(usuarioViewModel);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            ViewBag.Empresas = _empresaApp.GetAll();
            return View();
        }

        // POST: Usuarios/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UsuarioViewModel usuarioView)
        {
            if (ModelState.IsValid)
            {
                var usuarioDomain = Mapper.Map<UsuarioViewModel, Usuario>(usuarioView);
                _usuarioApp.Add(usuarioDomain);
                return RedirectToAction("Index");
            }
            else
                return View(usuarioView);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int id)
        {
            var usuario = _usuarioApp.GetbyId(id);
            var usuarioViewModel = Mapper.Map<Usuario, UsuarioViewModel>(usuario);

            ViewBag.Empresas = _empresaApp.GetAll();

            return View(usuarioViewModel);
        }

        // POST: Usuarios/Edit/5
        [HttpPost]
        public ActionResult Edit(UsuarioViewModel usuarioView)
        {
            if (ModelState.IsValid)
            {
                var usuarioDomain = Mapper.Map<UsuarioViewModel, Usuario>(usuarioView);
                _usuarioApp.Update(usuarioDomain);
                return RedirectToAction("Index");
            }
            return View(usuarioView);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int id)
        {
            var usuario = _usuarioApp.GetbyId(id);
            var usuarioViewModel = Mapper.Map<Usuario, UsuarioViewModel>(usuario);

            return View(usuarioViewModel);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfimed(int id)
        {
            var usuario = _usuarioApp.GetbyId(id);
            _usuarioApp.Remove(usuario);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void UploadImage()
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadImage"];
                if (httpPostedFile == null) return;

                var fileSave = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/Usuarios"), httpPostedFile.FileName);

                httpPostedFile.SaveAs(fileSave);
                var usuarioDomain = _usuarioApp.GetbyId((int)HttpRuntime.Cache.Get("Codigo"));
                System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(usuarioDomain.Foto));

                usuarioDomain.Foto = "/Images/Usuarios/" + httpPostedFile.FileName;
                _usuarioApp.Update(usuarioDomain);
                HttpRuntime.Cache.Insert("Foto", usuarioDomain.Foto);
            }
        }


    }
}
