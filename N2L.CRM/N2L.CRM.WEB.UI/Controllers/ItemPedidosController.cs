﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;

namespace N2L.CRM.WEB.UI.Controllers
{
    [Authorize]
    public class ItemPedidosController : Controller
    {
        private readonly IItemPedidoAppService _itemPedidoApp;
        private readonly IProdutoAppService _produtoApp;
        private readonly IProdutoUsuarioAppService _produtoUsuarioApp;
        private readonly IClienteAppService _clienteApp;


        private List<ItemPedido> itemPedidos = new List<ItemPedido>();

        public ItemPedidosController(IItemPedidoAppService itemPedidoApp,
                                     IProdutoAppService produtoApp,
                                     IProdutoUsuarioAppService produtoUsuarioApp,
                                     IClienteAppService clienteApp)
        {
            this._itemPedidoApp = itemPedidoApp;
            this._produtoApp = produtoApp;
            this._produtoUsuarioApp = produtoUsuarioApp;
            this._clienteApp = clienteApp;
        }

        // GET: ItemPedidos
        public ActionResult Index()
        {
            //var itemPedidoViewModel = Mapper.Map<IEnumerable<>>()

            return View();
        }

        // GET: ItemPedidos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ItemPedidos/Create
        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }
    
        // POST: ItemPedidos/Create
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(string produto, string cliente, string quantidade)
        {
            if (String.IsNullOrEmpty(produto)
              || String.IsNullOrEmpty(cliente)
              || String.IsNullOrEmpty(quantidade)
            )
            {
                Response.StatusCode = 999;
                return Content("verifique as informações");
            }
            else
            {
                var items = (List<ItemPedido>)Session["items"] ?? new List<ItemPedido>();
                var pedido = new Pedido();

              

                items.Add(new ItemPedido()
                {
                    Produto = _produtoApp.GetbyId(Convert.ToInt32(produto)),
                    ProdutoQtde = Convert.ToDecimal(quantidade)
                });
                Session["items"] = items;
                var itemProduto = _produtoUsuarioApp.BuscaPorProdutos(Convert.ToInt32(produto), (Usuario)Session["usuario"]).FirstOrDefault();
                itemProduto.QtdEstoque = itemProduto.QtdEstoque - Convert.ToInt32(quantidade);
                _produtoUsuarioApp.Update(itemProduto);
                var item = items.LastOrDefault();
                return Json(item, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: ItemPedidos/Edit/5
        public ActionResult Edit(int id)
        {
            var itemPedio = _itemPedidoApp.GetbyId(id);
            var itemPedidoViewModel = Mapper.Map<ItemPedido, ItemPedidoViewModel>(itemPedio);
            return View(itemPedidoViewModel);
        }

        // POST: ItemPedidos/Edit/5
        [HttpPost]
        public ActionResult Edit(ItemPedidoViewModel itemPedidoView)
        {
            if (ModelState.IsValid)
            {
                var itemPedido = Mapper.Map<ItemPedidoViewModel, ItemPedido>(itemPedidoView);
                _itemPedidoApp.Update(itemPedido);
                return RedirectToAction("Index");
            }
            return View(itemPedidoView);
        }

        // GET: ItemPedidos/Delete/5
        public ActionResult Delete(int id)
        {
            var itempedido = _itemPedidoApp.GetbyId(id);
            var itemPedidoViewModel = Mapper.Map<ItemPedido, ItemPedidoViewModel>(itempedido);
            return View(itemPedidoViewModel);
        }

        // POST: ItemPedidos/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var itemPedido = _itemPedidoApp.GetbyId(id);
            _itemPedidoApp.Remove(itemPedido);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AutoCompleteProdutos(string term)
        {
            var produtoUsuarioDomain = _produtoUsuarioApp.BuscaPorNome(term, (Usuario)Session["Usuario"]).ToList();

            List<Produto> produtoss = new List<Produto>();

            foreach (var ProdutoUsuario in produtoUsuarioDomain)
            {
                produtoss.Add(ProdutoUsuario.Produto);
            }

            if (produtoUsuarioDomain != null)
            {
                var listaProdutosDomain = produtoss;
                var produtos = Mapper.Map<IEnumerable<Produto>, IEnumerable<ProdutoViewModel>>(listaProdutosDomain);
                var arr = produtos.Select(x => new { Nome = x.Descricao, Codigo = x.ProdutoId }).ToList();
                return Json(arr, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content("Produto inválido");
            }

        }

        public ActionResult AutoCompleteClientes(string term)
        {
            var clientesDomain = _clienteApp.BuscaPorClienteUsuario(term, (Usuario)Session["usuario"]);

            if (clientesDomain != null)
            {
                var clientesViewModel = Mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteViewModel>>(clientesDomain);

                var json = clientesViewModel.Select(x => new { Nome = x.Nome, Codigo = x.ClienteId }).ToList();

                return Json(json, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content("Cliente não encontrado");
            }

        }

        public ActionResult GetDisponibility(int id, string quantidade)
        {

            if (id <= 0 || quantidade == "0")
            {
                return Content("Produto fora de estoque");
            }

            var qtd = Convert.ToDecimal(quantidade);
            var produtoUsuarioDomain = _produtoUsuarioApp.BuscaPorProdutos(id, (Usuario)Session["Usuario"]).FirstOrDefault();
            ProdutoViewModel produtoViewModel;


            if (qtd <= produtoUsuarioDomain.QtdEstoque)
            {
                produtoViewModel = Mapper.Map<Produto, ProdutoViewModel>(produtoUsuarioDomain.Produto);
                var itemPedidoViewModel = new ItemPedidoViewModel()
                {
                    Produto = produtoViewModel
                };
                Response.StatusCode = 998;
                TempData["disponivel"] = true;
                return Content("Produto Inserido");
            }
            else
            {
                Response.StatusCode = 999;
                TempData["disponivel"] = false;
                return Content("Produto fora de estoque");
            }

        }
    }
}
