﻿using System.IO;
using System.Linq;
using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;

namespace N2L.CRM.WEB.UI.Controllers
{
    [Authorize]
    public class ClientesController : Controller
    {

        private readonly IClienteAppService _clienteApp;

        public ClientesController(IClienteAppService clienteApp)
        {
            _clienteApp = clienteApp;
        }

        // GET: Clientes
        public ActionResult Index()
        {
            var clienteViewModel = Mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteViewModel>>(_clienteApp.GetAll());

            return View(clienteViewModel);
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int id)
        {
            var cliente = _clienteApp.GetbyId(id);
            var clienteViewModel = Mapper.Map<Cliente, ClienteViewModel>(cliente);

            return View(clienteViewModel);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClienteViewModel clienteView)
        {
            if (ModelState.IsValid)
            {
                var clienteDomain = Mapper.Map<ClienteViewModel, Cliente>(clienteView);
                _clienteApp.Add(clienteDomain);
                return RedirectToAction("Index");
            }
            return View(clienteView);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int id)
        {

            var cliente = _clienteApp.GetbyId(id);
            var clienteViewModel = Mapper.Map<Cliente, ClienteViewModel>(cliente);

            return View(clienteViewModel);
        }

        // POST: Clientes/Edit/5
        [HttpPost]
        public ActionResult Edit(ClienteViewModel cliente)
        {
            if (ModelState.IsValid)
            {
                var clienteDomain = Mapper.Map<ClienteViewModel, Cliente>(cliente);
                var usuario = (Usuario) Session["usuario"];
                clienteDomain.UsuarioId = usuario.UsuarioId;
                _clienteApp.Update(clienteDomain);
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int id)
        {

            var cliente = _clienteApp.GetbyId(id);
            var clienteViewModel = Mapper.Map<Cliente, ClienteViewModel>(cliente);

            return View(clienteViewModel);
        }

        // POST: Clientes/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var cliente = _clienteApp.GetbyId(id);
            _clienteApp.Remove(cliente);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void UploadImage()
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadImage"];
                if (httpPostedFile != null)
                {
                    var fileSave = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/Clientes"),
                        httpPostedFile.FileName);
                    httpPostedFile.SaveAs(fileSave);

                }
            }
        }
    }
}
