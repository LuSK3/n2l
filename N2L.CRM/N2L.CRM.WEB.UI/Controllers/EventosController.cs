﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;
using N2L.CRM.Domain.Services;
using N2L.CRM.WEB.UI.ViewModels;

namespace N2L.CRM.WEB.UI.Controllers
{
    [Authorize]
    public class EventosController : Controller
    {
        private readonly IEventoService _eventoService;
        private readonly IUsuarioService _usuarioService;

        public EventosController(IEventoService eventoService, IUsuarioService usuarioService)
        {
            this._eventoService = eventoService;
            this._usuarioService = usuarioService;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public JsonResult GetListEventos(string start, string end)
        {

            var dataInicial = UnixTimeStampToDateTime(Convert.ToDouble(start));
            var dataFinal = UnixTimeStampToDateTime(Convert.ToDouble(end));


            var eventoViewModel = Mapper.Map<IEnumerable<Eventos>, IEnumerable<EventosViewModel>>(_eventoService.GetEventosUsers(new Eventos()
            {
                Usuario = (Usuario)Session["usuario"],
                DtEventoInicio = dataInicial,
                DtEventoFinal = dataFinal
            }));

            var eventos = eventoViewModel.Select(e => new
            {
                id = e.EventoId,
                title = e.Descricao,
                start = e.DtEventoInicio.ToString("yyyy-MM-ddTHH:mm:ssZ").Replace("T00:00:00Z", string.Empty),
                end = e.DtEventoFinal != null ? e.DtEventoFinal.ToString("yyyy-MM-ddTHH:mm:ssZ") : string.Empty,
                color = e.Color,
                allDay = false
            });

            return Json(eventos, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void AtualizarEventos(string id, string newEventStart, string newEventEnd, string visualizacaoAtual)
        {
            DateTime? dataFinal = null;

            if (visualizacaoAtual.Equals("month"))
            {
                if (newEventStart.Contains("T00:00:00.000Z"))
                    newEventStart = newEventStart.Replace("T00:00:00.000Z", string.Empty);

                if (!string.IsNullOrEmpty(newEventEnd))
                {
                    if (newEventEnd.Contains("T00:00:00.000z"))
                    {
                        newEventEnd = newEventEnd.Replace("T00:00:00.000z", string.Empty);
                        dataFinal = Convert.ToDateTime(newEventEnd);
                    }
                }
            }
            newEventStart = newEventStart.Replace(".000z", string.Empty);
            if (!string.IsNullOrEmpty(newEventEnd))
            {
                newEventEnd = newEventEnd.Replace(".000z", string.Empty);
                dataFinal = Convert.ToDateTime(newEventEnd);
            }

            DateTime dataInicial = Convert.ToDateTime(newEventStart);

            var eventoDomain = _eventoService.GetbyId(Convert.ToInt32(id));
            eventoDomain.DtEventoInicio = dataInicial;
            eventoDomain.DtEventoFinal = Convert.ToDateTime(dataFinal);
            _eventoService.Update(eventoDomain);
        }

        [HttpPost]
        public ActionResult EditarEventos(string id, string descricao, string dataInicio, 
                                          string dataFinal, string color)
        {
            var usuario = (Usuario)Session["usuario"];

            var eventoViewModel = new EventosViewModel()
            {
                EventoId = Convert.ToInt32(id),
                Descricao = descricao,
                DtEventoInicio = Convert.ToDateTime(dataInicio),
                DtEventoFinal = Convert.ToDateTime(dataFinal),
                Color = "#" + color,
                EmpresaId = usuario.EmpresaId,
                UsuarioId = usuario.UsuarioId
            };

            var evento = Mapper.Map<EventosViewModel, Eventos>(eventoViewModel);

            _eventoService.Update(evento);
            return Content("");
        }      
        [HttpPost]
        public ActionResult AdicionarEventos(string id, string descricao, string dataInicio,
                                          string dataFinal, string color)
        {
            var usuario = (Usuario) Session["usuario"];


            var eventoViewModel = new EventosViewModel()
            {
                Descricao = descricao,
                DtEventoInicio = Convert.ToDateTime(dataInicio),
                DtEventoFinal = Convert.ToDateTime(dataFinal),
                Color = "#"+color,
                EmpresaId = usuario.EmpresaId,
                UsuarioId = usuario.UsuarioId
                
            };

            var evento = Mapper.Map<EventosViewModel, Eventos>(eventoViewModel);

            _eventoService.Add(evento);
            return Content("");
        }           
        
        [HttpGet]
        public JsonResult GetEvento(string id)
        {
            return Json(_eventoService.GetbyId(Convert.ToInt32(id)), JsonRequestBehavior.AllowGet);

        }    
    }                                                
}
