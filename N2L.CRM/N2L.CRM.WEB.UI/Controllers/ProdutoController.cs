﻿using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace N2L.CRM.WEB.UI.Controllers
{
    [Authorize]
    public class ProdutoController : Controller
    {
        private readonly IEmpresaAppService _empresaApp;
        private readonly IProdutoAppService _produtoApp;

        public ProdutoController(IProdutoAppService produtoAppService, IEmpresaAppService empresaAppService)
        {
            _empresaApp = empresaAppService;
            _produtoApp = produtoAppService;
        }

        // GET: Produto
        public ActionResult Index()
        {
            var produtoViewModel = Mapper.Map<IEnumerable<Produto>, IEnumerable<ProdutoViewModel>>(_produtoApp.GetAll());
            return View(produtoViewModel);
        }

        // GET: Produto/Details/5
        public ActionResult Details(int id)
        {
            var produto = _produtoApp.GetbyId(id);
            var produtoViewModel = Mapper.Map<Produto, ProdutoViewModel>(produto);

            return View(produtoViewModel);
        }

        // GET: Produto/Create
        public ActionResult Create()
        {
            ViewBag.Empresas = _empresaApp.GetAll();
            return View();
        }

        // POST: Produto/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProdutoViewModel produtoView)
        {
            if (ModelState.IsValid)
            {
                var produto = Mapper.Map<ProdutoViewModel, Produto>(produtoView);
                _produtoApp.Add(produto);
                return RedirectToAction("Index");
            }
            return View(produtoView);
        }                                                   

        // GET: Produto/Edit/5
        public ActionResult Edit(int id)
        {
            var produto = _produtoApp.GetbyId(id);
            var produtoViewModel = Mapper.Map<Produto, ProdutoViewModel>(produto);
            ViewBag.Empresas = _empresaApp.GetAll();
            return View(produtoViewModel);
        }

        // POST: Produto/Edit/5
        [HttpPost]
        public ActionResult Edit(ProdutoViewModel produtoView)
        {
            if (ModelState.IsValid)
            {
                var produto = Mapper.Map<ProdutoViewModel, Produto>(produtoView);
                _produtoApp.Update(produto);
                return RedirectToAction("Index");
            }
            return View(produtoView);
        }

        // GET: Produto/Delete/5
        public ActionResult Delete(int id)
        {
            var produto = _produtoApp.GetbyId(id);
            var produtoViewModel = Mapper.Map<Produto, ProdutoViewModel>(produto);
            return View(produtoViewModel);
        }

        // POST: Produto/Delete/5
        [HttpPost,ActionName("Delete")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var produto = _produtoApp.GetbyId(id);
            _produtoApp.Remove(produto);
            return RedirectToAction("Index");
        }
    }
}
