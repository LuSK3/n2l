﻿using AutoMapper;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace N2L.CRM.WEB.UI.Controllers
{
    [Authorize]
    public class EmpresaController : Controller
    {
        private readonly IEmpresaAppService _empresaApp;

        public EmpresaController(IEmpresaAppService empresaAppService)
        {
            _empresaApp = empresaAppService;
        }

        // GET: Empresa
        public ActionResult Index()
        {
            var empresaViewModel = Mapper.Map<IEnumerable<Empresa>,IEnumerable<EmpresaViewModel>>(_empresaApp.GetAll());
            return View(empresaViewModel);
        }

        // GET: Empresa/Details/5
        public ActionResult Details(int id)
        {
            var empresa = _empresaApp.GetbyId(id);
            var empresaViewModel = Mapper.Map<Empresa, EmpresaViewModel>(empresa);
            return View(empresaViewModel);
        }

        // GET: Empresa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Empresa/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmpresaViewModel empresaViewModel)
        {
            if (ModelState.IsValid)
            {
                var empresaDomain = Mapper.Map<EmpresaViewModel, Empresa>(empresaViewModel);
                _empresaApp.Add(empresaDomain);
                return RedirectToAction("Index");
            }
            return View(empresaViewModel);
        }

        // GET: Empresa/Edit/5
        public ActionResult Edit(int id)
        {
            var empresa = _empresaApp.GetbyId(id);
            var empresaViewModel = Mapper.Map<Empresa, EmpresaViewModel>(empresa);

            return View(empresaViewModel);
        }

        // POST: Empresa/Edit/5
        [HttpPost]
        public ActionResult Edit(EmpresaViewModel empresaView)
        {
            if (ModelState.IsValid)
            {
                var empresaDomain = Mapper.Map<EmpresaViewModel, Empresa>(empresaView);
                 _empresaApp.Update(empresaDomain);
                return RedirectToAction("Index");
            }
            return View(empresaView);
        }

        // GET: Empresa/Delete/5
        public ActionResult Delete(int id)
        {
            var empresa = _empresaApp.GetbyId(id);
            var empresaViewModel = Mapper.Map<Empresa, EmpresaViewModel>(empresa);
            return View(empresaViewModel);
        }

        // POST: Empresa/Delete/5
        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var empresaDomain = _empresaApp.GetbyId(id);
            _empresaApp.Remove(empresaDomain);
            return RedirectToAction("Index");
        }
    }
}
