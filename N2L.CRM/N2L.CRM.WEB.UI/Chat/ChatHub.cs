﻿using Microsoft.AspNet.SignalR;
using System;
using System.Threading.Tasks;
using N2L.CRM.Application.Interface;

namespace N2L.CRM.WEB.UI.Chat
{
    public class ChatHub : Hub
    {
        //private readonly IUsuarioAppService _usuarioApp;
        //public ChatHub(IUsuarioAppService usuarioApp)
        //{
        //    this._usuarioApp = usuarioApp;
        //}

        public void Send(string codigo, string message)
        {
            //var usuarioDomain = _usuarioApp.GetbyId(Convert.ToInt32(codigo));
            
            Clients.All.broadCastMessage(codigo, message);
        }
    }
}