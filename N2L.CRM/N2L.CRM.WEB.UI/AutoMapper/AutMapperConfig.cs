﻿using AutoMapper;

namespace N2L.CRM.WEB.UI.AutoMapper
{
    public class AutMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDomainMappingProfile>();
            });   
        }
    }
}