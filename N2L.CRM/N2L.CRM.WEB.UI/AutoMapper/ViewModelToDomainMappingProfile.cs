﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using N2L.CRM.Domain.Entities;
using N2L.CRM.WEB.UI.ViewModels;

namespace N2L.CRM.WEB.UI.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomaintoViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Cliente, ClienteViewModel>();
            Mapper.CreateMap< Pedido, PedidoViewModel >();
            //Mapper.CreateMap<ItemPedido,ItemPedidoViewModel>();
            Mapper.CreateMap<Produto, ProdutoViewModel>();
            Mapper.CreateMap<Usuario, UsuarioViewModel>();
            Mapper.CreateMap<Empresa, EmpresaViewModel>();
            Mapper.CreateMap<Eventos, EventosViewModel>();
        }
    }
}