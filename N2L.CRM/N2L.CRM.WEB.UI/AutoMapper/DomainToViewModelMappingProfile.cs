﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using N2L.CRM.WEB.UI.ViewModels;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.WEB.UI.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<ClienteViewModel, Cliente>();
            Mapper.CreateMap<PedidoViewModel, Pedido>();
            //Mapper.CreateMap<ItemPedidoViewModel, ItemPedido>();
            Mapper.CreateMap<ProdutoViewModel, Produto>();
            Mapper.CreateMap<UsuarioViewModel, Usuario>();
            Mapper.CreateMap<EmpresaViewModel, Empresa>();
            Mapper.CreateMap<EventosViewModel, Eventos>();
        }

    }
}