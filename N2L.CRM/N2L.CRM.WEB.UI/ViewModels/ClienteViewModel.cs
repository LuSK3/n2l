﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using N2L.CRM.WEB.UI.EnumView;
using System.ComponentModel.DataAnnotations;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class ClienteViewModel
    {
        [Key]
        public int ClienteId { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(150,ErrorMessage = "Maximo de {0} caractéres")]
        [MinLength(2,ErrorMessage = "Mínimo {0} caractéres")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Preencha o campo Sobrenome")]
        [MaxLength(150, ErrorMessage = "Maximo de {0} caractéres")]
        [MinLength(2, ErrorMessage = "Mínimo {0} caractéres")]
        public string Sobrenome { get; set; }
        public DateTime DataNascimento { get; set; }

        [Required(ErrorMessage = "Preencha o campo Email")]
        [MaxLength(100, ErrorMessage = "Maximo de {0} caractéres")]
        [EmailAddress(ErrorMessage = "Preencha um email válido")]
        [DisplayName("E-mail")]
        public string Email { get; set; }
        public ClienteViewModel Anfitriã { get; set; }
        public string Observacao { get; set; }


        [DisplayName("Período Contato")]
        public PeriodoEnumView PeriodoContato { get; set; }
        [DisplayName("Tipo Contato")]
        public TipoContatoEnumView TipoContato { get; set; }
        [DisplayName("Interesse Produto")]
        public InteresseProdutoEnumVIew InteresseProduto { get; set; }
        [DisplayName("Tom da Pele")]
        public TomDaPeleEnumView TomDaPele { get; set; }
        public TipoPeleEnumView TipoPele { get; set; }


        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Facebook { get; set; }


        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string Pais { get; set; }
        public bool Status { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataCadastro { get; set; }

        public virtual IEnumerable<PedidoViewModel> Pedidos { get; set; }
        public int UsuarioId { get; set; }
        public virtual UsuarioViewModel Usuario { get; set; }
    }
}