﻿using System;
using System.ComponentModel.DataAnnotations;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class EventosViewModel
    {
        public int EventoId { get; set; }
        public int UsuarioId { get; set; }
        public int EmpresaId { get; set; }
        public string Descricao { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DtEventoInicio { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DtEventoFinal { get; set; }
        public bool Status { get; set; }
        public string Color { get; set; }
        public virtual UsuarioViewModel Usuario { get; set; }
        public virtual EmpresaViewModel Empresa { get; set; }
    }
}