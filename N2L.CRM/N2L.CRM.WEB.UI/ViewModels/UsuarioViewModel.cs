﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using N2L.CRM.Application;
using N2L.CRM.Application.Interface;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class UsuarioViewModel
    {
         
        [Key]
        public int UsuarioId { get; set; }

        public string Nome { get; set; }

        [Required(ErrorMessage = "Informe Login")]
        [Remote("ValidaLogin","Validacao")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe Senha")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Description("Número de Registro da Empresa Vinculada")]
        [Required(ErrorMessage = "Informe sua Matrícula")]
        [Display(Name = "Matrícula")]
        public string Matricula { get; set; }

        [Description("Endereço de Contato")]
        [Required(ErrorMessage = "Informe o endereço")]
        public string Endereco { get; set; }

        [Description("Bairro de Contato")]
        [Required(ErrorMessage = "Informe o Bairro")]
        public string Bairro { get; set; }

        [Description("Cidade de Contato")]
        [Required(ErrorMessage = "Informe a Cidade")]
        public string Cidade { get; set; }

        [Description("Estado de Contato")]
        [Required(ErrorMessage = "Informe o Estado")]
        public string UF { get; set; }
        public string Pais { get; set; }

        [Description("Telefone Fixo para contato")]
        [Required(ErrorMessage = "Informe um telefone fixo")]
        public string Telefone { get; set; }
        
        [DisplayName("Celular")]
        public string Celular { get; set; }         
        [Required(ErrorMessage = "Informe seu perfil do Facebook")]
        public string Facebook { get; set; }

        [Required(ErrorMessage = "Informe um e-mail")]
        [DisplayName("E-mail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Foto { get; set; }

        [Required(ErrorMessage = "Informe Matrícula do Supervisor")]
        [DisplayName("Supervisor")]
        public int CodigoSupervisor { get; set; }

        [ScaffoldColumn(false)]
        public int EmpresaId { get; set; }
        public EmpresaViewModel Empresa { get; set; }





    }
}