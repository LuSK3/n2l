﻿
using System.ComponentModel;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class ProdutoUsuarioViewModel
    {
        public int ProdutoUsuarioId { get; set; }
        [DisplayName("Estoque")]
        public decimal QtdEstoque { get; set; }
        [DisplayName("Código Produto")]
        public int ProdutoId { get; set; }
        [DisplayName("Código Usuário")]
        public int UsuarioId { get; set; }
        public virtual ProdutoViewModel Produto { get; set; }
        public virtual UsuarioViewModel Usuario { get; set; }
    }
}