﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class ProdutoViewModel
    {
        [Key]
        public int ProdutoId { get; set; }

        [Required(ErrorMessage = "Informe uma descrição")]
        [MaxLength(200, ErrorMessage = "Maximo de {0} caractéres")]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(decimal), "0", "999999999999")]
        [Required(ErrorMessage = "Informe uma Valor para o Produto")]
        public decimal Valor { get; set; }

        [DisplayName("Disponível?")]
        public bool Disponivel { get; set; }

        [DisplayName("Empresa")]
        public int EmpresaId { get; set; }

        [DisplayName("Usuario")]
        public int UsuarioId { get; set; }

        public virtual UsuarioViewModel Usuario { get; set; }

        public virtual EmpresaViewModel Empresa { get; set; }

    }
}