﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class PedidoViewModel
    {
        [Key]
        public int PedidoId { get; set; }
        public int ClienteId { get; set; }
        public string Status { get; set; }
        public decimal ProdutoQtde { get; set; }
        public DateTime DataPedido { get; set; }
        public DateTime DataEntrega { get; set; }
        //public virtual IEnumerable<ItemPedido> ItemPedidos { get; set; }
        public int UsuarioId { get; set; }
        public virtual UsuarioViewModel Usuario { get; set; }
        public int ProdutoId { get; set; }
        public virtual ProdutoViewModel Produto { get; set; }
        public virtual ClienteViewModel Cliente { get; set; }
    }
}