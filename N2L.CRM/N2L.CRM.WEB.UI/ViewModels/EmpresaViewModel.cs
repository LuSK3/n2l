﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class EmpresaViewModel
    {
        [Key]
        public int EmpresaId { get; set; }

        [Required(ErrorMessage = "Informe o Nome da Empresa")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Informe o CNPJ da Empresa")]
        public string CNPJ { get; set; }


        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Pais { get; set; }
        public string CEP { get; set; }
        public string Telefone { get; set; }
        public string TelefoneRecado { get; set; }
        public string Site { get; set; }


        public virtual IEnumerable<UsuarioViewModel> Usuarios { get; set; }
    }
}