﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace N2L.CRM.WEB.UI.ViewModels
{
    public class ItemPedidoViewModel
    {
        [Key]
        public int ItemPedidoId { get; set; }


        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "Informe a Quantidade")]
        [Range(typeof(decimal),"0","999999999999")]
        [DisplayName("Quantidade")]
        public decimal ProdutoQtde { get; set; }

        [DisplayName("Produto")]
        [Required(ErrorMessage = "Informe um Produto")]
        public int ProdutoId { get; set; }

        [DisplayName("Produto")]
        public virtual ProdutoViewModel Produto { get; set; }

        [DisplayName("Código Pedido")]
        public int PedidoId { get; set; }

        public virtual PedidoViewModel Pedido { get; set; }
    }
}