﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace N2L.CRM.WEB.UI.EnumView
{
    public enum TipoPeleEnumView : byte
    {
        Seca,
        Mista,
        Oleosa
    }
}