﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace N2L.CRM.WEB.UI.EnumView
{
    public enum TipoContatoEnumView:byte
    {
        Telefone,
        [Description("E-Mail")]
        Email,
        Pessoalmente,
        [Description("Mídias Sociais")]
        MidiasSociais
    }
}