﻿function initialize() {
    var mapCanvas = document.getElementById('mapa');
    var mapOptions = {
        center: new google.maps.LatLng(-23.5564712, -46.1857536),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{ "stylers": [{ "hue": "#dd0d0d" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "lightness": 100 }, { "visibility": "simplified" }] }]
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
}
google.maps.event.addDomListener(window, 'load', initialize);

$(".dropdown-menu").on('click', 'li a', function () {
    var classeBotao = $(this).attr("class");
    $(".btn-selecione-ddl-cor-evento")
.removeClass("btn-default btn-success btn-danger btn-info btn-warning");
    $(".btn-selecione-ddl-cor-evento").addClass("btn-" + classeBotao).text($(this).text()).val($(this).text());

    var valorHiddenCor = "";
    switch (classeBotao) {
        case "primary":
            valorHiddenCor = "337ab7";
            break;
        case "success":
            valorHiddenCor = "5cb85c";
            break;
        case "danger":
            valorHiddenCor = "c9302c";
            break;
        case "warning":
            valorHiddenCor = "f0ad4e";
            break;
        case "info":
            valorHiddenCor = "5bc0de";
            break;
    }
    $("#hdf-cor-evento-edicao").val(valorHiddenCor);
});

function ObterDataAtual() {

    var date = new Date();
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();
    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
};

function atualizarEvento(eventId, eventStart, eventEnd) {
    var visualizacaoAtual = $('#calendar').fullCalendar('getView').name;
    var dataRow = {
        'id': eventId,
        'newEventStart': eventStart,
        'newEventEnd': eventEnd,
        'visualizacaoAtual': visualizacaoAtual
    };
    $.ajax({
        type: 'POST',
        url: '/Eventos/AtualizarEventos',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(dataRow)
    });
}

function cleanDate(d) { return new Date(+d.replace(/\/Date\((\d+)\)\//, '$1')); }

function IniciarEventos() {
    $('#external-events .fc-event').each(function () {
        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });
        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
}

$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
    },
    defaultDate: ObterDataAtual(),
    dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outrubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    allDayText: 'Dia inteiro',
    buttonText: {
        today: 'Hoje',
        month: 'Mês',
        week: 'Semana',
        day: 'Dia'
    },
    events: '/Eventos/GetListEventos',
    droppable: true,
    drop: function (date) {
        $(this).remove();
        var titulo = $(this).html();
        $.ajax({
            type: 'POST',
            url: '/Eventos/AdicionarEventos',
            data: { title: titulo, start: date },
            dataType: 'json',
            success: function () {
                $('#calendar').fullCalendar("removeEvents");
                $('#calendar').fullCalendar("refetchEvents");
            }
        });
    },
    eventClick: function (callEvent) {
        $.ajax({
            url: '/Eventos/GetEvento',
            data: { id: callEvent.id },
            type: 'GET',
            success: function (data) {
                console.log(data);

                var dataInicio = data.DtEventoInicio;
                var dataFinal = data.DtEventoFinal;

                var horaInicio = ("0" + (cleanDate(dataInicio)).getHours()).slice(-2) + ":" + ("0" + (cleanDate(dataInicio)).getMinutes()).slice(-2);
                var horaFinal = ("0" + (cleanDate(dataFinal)).getHours()).slice(-2) + ":" + ("0" + (cleanDate(dataInicio)).getMinutes()).slice(-2);

                dataInicio = ((cleanDate(dataInicio).getDate()) < "10" ? "0" + (cleanDate(dataInicio)).getDate() : (cleanDate(dataInicio)).getDate()) + "/" + ((cleanDate(dataInicio).getMonth() + 1) < "10" ? "0" + (cleanDate(dataInicio).getMonth() + 1) : (cleanDate(dataInicio)).getMonth() + 1) + "/" + (cleanDate(dataInicio)).getFullYear();

                dataFinal = ((cleanDate(dataFinal).getDate()) < "10" ? "0" + (cleanDate(dataFinal)).getDate() : (cleanDate(dataFinal)).getDate()) + "/" + ((cleanDate(dataFinal).getMonth() + 1) < "10" ? "0" + (cleanDate(dataFinal).getMonth() + 1) : (cleanDate(dataFinal)).getMonth() + 1) + "/" + (cleanDate(dataFinal)).getFullYear();

                $('#EventoId').val(data.EventoId);
                $('#Descricao').val(data.Descricao);

                $('#dataInicio').val(dataInicio);
                $('#dataFinal').val(dataFinal);

                $('#horaInicio').val(horaInicio);
                $('#horaFinal').val(horaFinal);

                $('.modal-editar-evento').modal('show');
            }
        });
    },
    dayClick: function (date, jsEvent, view) {
        $('#EventoId').val('0');
        $('#Descricao').val('');
        //$('#dataInicio').val();
        $('#horaInicio').val('00:00');

        //$('#dataInicio').val(new Date(date * 1000));
        $('#horaFinal').val('00:00');

        $('.modal-editar-evento').modal('show');
    },
    editable: true,
    eventDrop: function (event) {
        atualizarEvento(event.id, event.start, event.end);
    },
    eventResize: function (event) {
        atualizarEvento(event.id, event.start, event.end);
    }
});

