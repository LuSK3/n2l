using N2L.CRM.Application;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Interfaces.Repositories;
using N2L.CRM.Domain.Interfaces.Services;
using N2L.CRM.Domain.Services;
using N2L.CRM.Infra.Data.Repositories;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(N2L.CRM.WEB.UI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(N2L.CRM.WEB.UI.App_Start.NinjectWebCommon), "Stop")]

namespace N2L.CRM.WEB.UI.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            kernel.Bind<IClienteAppService>().To<ClienteAppService>();
            kernel.Bind<IProdutoAppService>().To<ProdutoAppService>();
            //kernel.Bind<IItemPedidoAppService>().To<ItemPedidoAppService>();
            kernel.Bind<IPedidoAppService>().To<PedidoAppService>();
            kernel.Bind<IUsuarioAppService>().To<UsuarioAppService>();
            kernel.Bind<IEmpresaAppService>().To<EmpresaAppService>();
            kernel.Bind<IProdutoUsuarioAppService>().To<ProdutoUsuarioAppService>();
            kernel.Bind<IEventoAppService>().To<EventoAppService>();

            kernel.Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            kernel.Bind<IClienteService>().To<ClienteService>();
            kernel.Bind<IProdutoService>().To<ProdutoService>();
            //kernel.Bind<IItemPedidoService>().To<ItemPedidoService>();
            kernel.Bind<IPedidoService>().To<PedidoService>();
            kernel.Bind<IUsuarioService>().To<UsuarioService>();
            kernel.Bind<IEmpresaService>().To<EmpresaService>();
            kernel.Bind<IProdutoUsuarioService>().To<ProdutoUsuarioService>();
            kernel.Bind<IEventoService>().To<EventoService>();

            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IClienteRepository>().To<ClienteRepository>();
            kernel.Bind<IProdutoRepository>().To<ProdutoRepository>();
            //kernel.Bind<IItemPedidoRepository>().To<ItemPedidoRepository>();
            kernel.Bind<IPedidoRepository>().To<PedidoRepository>();
            kernel.Bind<IUsuarioRepository>().To<UsuarioRepository>();
            kernel.Bind<IEmpresaRepository>().To<EmpresaRepository>();
            kernel.Bind<IProdutoUsuarioRepository>().To<ProdutoUsuarioRepository>();
            kernel.Bind<IEventoRepository>().To<EventoRepository>();
        }
    }
}
