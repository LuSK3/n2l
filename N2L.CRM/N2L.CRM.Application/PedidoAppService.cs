﻿using System.Linq;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace N2L.CRM.Application
{
    public class PedidoAppService:AppServiceBase<Pedido>,IPedidoAppService
    {
        private readonly IPedidoService _pedidoService;

        public PedidoAppService(IPedidoService pedidoService)
            :base(pedidoService)
        {
            _pedidoService = pedidoService;
        }

        public IEnumerable<Pedido> BuscaPedidosUsuario(Usuario usuario)
        {
            return _pedidoService.BuscaPedidosUsuario(usuario);
        }
    }
}
