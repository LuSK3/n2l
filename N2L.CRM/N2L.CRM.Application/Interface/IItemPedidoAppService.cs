﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Application.Interface
{
    public interface IItemPedidoAppService :IAppServiceBase<ItemPedido>
    {
    }
}
