﻿using System.Collections;
using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Application.Interface
{
    public interface IClienteAppService  :IAppServiceBase<Cliente>
    {
        IEnumerable<Cliente> BuscaPorClienteUsuario(string nome, Usuario usuario);

    }
}
