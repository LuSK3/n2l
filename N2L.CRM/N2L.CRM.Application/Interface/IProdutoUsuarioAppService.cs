﻿using N2L.CRM.Domain.Entities;
using System.Collections.Generic;

namespace N2L.CRM.Application.Interface
{
    public interface IProdutoUsuarioAppService :IAppServiceBase<ProdutoUsuario>
    {
        IEnumerable<ProdutoUsuario> BuscaPorUsuario(Usuario usuario);

        IEnumerable<ProdutoUsuario> BuscaPorProdutos(int id, Usuario usuario);

        IEnumerable<ProdutoUsuario> BuscaPorNome(string termo,Usuario usuario);
    }
}
