﻿using System.Collections.Generic;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Application.Interface
{
    public interface IEventoAppService : IAppServiceBase<Eventos>
    {
        IEnumerable<Eventos> GetEventosUsers(Eventos eventos);
    }
}
