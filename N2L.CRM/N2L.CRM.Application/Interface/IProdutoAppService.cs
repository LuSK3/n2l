﻿using N2L.CRM.Domain.Entities;
using System.Collections.Generic;

namespace N2L.CRM.Application.Interface
{
    public interface IProdutoAppService : IAppServiceBase<Produto>
    {
        IEnumerable<Produto> BuscaporNome(string nome);
    }
}
