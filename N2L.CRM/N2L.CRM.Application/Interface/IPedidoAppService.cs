﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Application.Interface
{
    public interface IPedidoAppService:IAppServiceBase<Pedido>
    {
        IEnumerable<Pedido> BuscaPedidosUsuario(Usuario usuario);
         
    }
}
