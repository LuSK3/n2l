﻿using N2L.CRM.Domain.Entities;

namespace N2L.CRM.Application.Interface
{
    public interface IUsuarioAppService:IAppServiceBase<Usuario>
    {
        Usuario BuscaPorLogin(string login, string senha);

        Usuario BuscaLogin(string Login);

       
    }
}
