﻿using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;



namespace N2L.CRM.Application
{
    public class UsuarioAppService:AppServiceBase<Usuario>,IUsuarioAppService
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioAppService(IUsuarioService usuarioService)
            :base(usuarioService)
        {
            _usuarioService = usuarioService;
        }

        public Usuario BuscaPorLogin(string nome, string senha)
        {
            
            return _usuarioService.BuscaPorLogin(nome, senha);
        }

        public Usuario BuscaLogin(string Login)
        {
            return _usuarioService.BuscaLogin(Login);

        }
    }
}
