﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Application
{
    public class ItemPedidoAppService : AppServiceBase<ItemPedido>, IItemPedidoAppService
    {
        private readonly IItemPedidoService _itemPedidoService;

        public ItemPedidoAppService(IItemPedidoService itemPedidoService)
            : base(itemPedidoService)
        {
            _itemPedidoService = itemPedidoService;
        }
    }
}
