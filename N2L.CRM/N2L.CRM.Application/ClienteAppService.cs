﻿using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Application
{
    public class ClienteAppService : AppServiceBase<Cliente>, IClienteAppService
    {
        private readonly IClienteService _clienteService;

        public ClienteAppService(IClienteService clienteService)
            : base(clienteService)
        {
            _clienteService = clienteService;
        }

        public System.Collections.Generic.IEnumerable<Cliente> BuscaPorClienteUsuario(string nome, Usuario usuario)
        {
            return _clienteService.BuscaClienteUsuario(nome, usuario);
        }
    }
}
