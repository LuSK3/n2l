﻿using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace N2L.CRM.Application
{
    public class EventoAppService : AppServiceBase<Eventos>, IEventoAppService
    {
        private readonly IEventoService _eventoService;

        public EventoAppService(IEventoService eventoService)
            : base(eventoService)
        {
            this._eventoService = eventoService;
        }

        public IEnumerable<Eventos> GetEventosUsers(Eventos eventos)
        {
            return _eventoService.GetEventosUsers(eventos);
        }
    }
}
