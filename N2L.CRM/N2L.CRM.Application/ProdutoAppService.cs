﻿using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;
using System.Collections.Generic;

namespace N2L.CRM.Application
{
    public class ProdutoAppService:AppServiceBase<Produto>,IProdutoAppService
    {

        private readonly IProdutoService _produtoService;

        public ProdutoAppService(IProdutoService produtoService)
            :base(produtoService)
        {
            _produtoService = produtoService;
        }

        public IEnumerable<Produto> BuscaporNome(string nome)
        {
            return _produtoService.BuscaPorNome(nome);
        }
    }
}
