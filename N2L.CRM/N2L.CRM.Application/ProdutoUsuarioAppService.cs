﻿using System.Collections.Generic;
using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Application
{
    public class ProdutoUsuarioAppService : AppServiceBase<ProdutoUsuario>, IProdutoUsuarioAppService
    {
        private readonly IProdutoUsuarioService _produtoUsuarioApp;
        public ProdutoUsuarioAppService(IProdutoUsuarioService produtoUsuarioApp)
            : base(produtoUsuarioApp)
        {
            this._produtoUsuarioApp = produtoUsuarioApp;
        }

        public IEnumerable<ProdutoUsuario> BuscaPorProdutos(int id, Usuario usuario)
        {
            return _produtoUsuarioApp.BuscaPorProduto(id, usuario);
        }

        public IEnumerable<ProdutoUsuario> BuscaPorUsuario(Usuario usuario)
        {
            return _produtoUsuarioApp.BuscaPorUsuario(usuario);
        }


        public IEnumerable<ProdutoUsuario> BuscaPorNome(string termo, Usuario usuario)
        {
            return _produtoUsuarioApp.BuscaPorNome(termo, usuario);
        }
    }
}
