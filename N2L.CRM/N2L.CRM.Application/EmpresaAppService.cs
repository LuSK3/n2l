﻿using N2L.CRM.Application.Interface;
using N2L.CRM.Domain.Entities;
using N2L.CRM.Domain.Interfaces.Services;

namespace N2L.CRM.Application
{
    public class EmpresaAppService:AppServiceBase<Empresa>,IEmpresaAppService
    {
        private readonly IEmpresaService _empresaService;

        public EmpresaAppService(IEmpresaService empresaService)
            :base(empresaService)
        {
            _empresaService = empresaService;
        }
    }
}
